create database recycle_management_system;

use recycle_management_system;

create table admin (id int(2) primary key, username varchar(20), password varchar(12));

insert into admin (id, username, password) values (1, 'ashwini','ashujp1998'),(2, 'saikumar','sai123'),(3, 'ashwini','ashujp1998'),(4,'mounika','mouni123'),(5,'rasoolkhan','hero'); 

create table user (firstname varchar(25), lastname varchar(25), email varchar(40), contactnumber varchar(10), password varchar(20), gender varchar(10));

create table manager (first_name varchar(20),last_name varchar(20),email varchar(200),phone_number varchar(100) ,password varchar(50) ,gender varChar(20),skills varchar(20),approved char(20));

create table vendor_request(request_id int (10), vendor_email varchar(200), type_of_org varchar(20), amount int(100), location varchar(20), request_date date, required_date date, status varchar(20), time time);

create table buyer_request(request_id int(10), buyer_email varchar(50), quantity int(20),amount int(20), location varchar(50), request_date date, required_date date, payment_date date, status varchar(20), paid_amount int(20));